<?php
/**
 * Plugin Name:       GM Academic
 * Plugin URI:        https://gitlab.com/nebulon42/gm-academic
 * Description:       Provides utilities for academic writing.
 * Version:           1.9.2
 * Requires at least: 6.2
 * Author:            Michael Glanznig
 * Text Domain:       gm-academic
 * Domain Path:       /languages
 * Author URI:        https://gitlab.com/nebulon42
 *
 * @package GM_Academic
 **/

// if we are not within WordPress we do nothing
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'GM_ACADEMIC_VERSION', '1.9.2' );
define( 'GM_ACADEMIC_BASE_URL', plugin_dir_url( __FILE__ ) );
define( 'GM_ACADEMIC_BASE_PATH', plugin_dir_path( __FILE__ ) );

// constant for the DB version option
define( 'GM_ACADEMIC_DB_VERSION', 'gm_academic_db_version' );

global $gm_academic_db_version;
$gm_academic_db_version = 1;

/**
 * Do things on plugin activation.
 */
register_activation_hook(
	__FILE__,
	function () {
		gm_academic_db_setup();
	}
);

/**
 * Init the plugin
 */
add_action(
	'init',
	function () {
		load_plugin_textdomain( 'gm-academic', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		gm_academic_pid_init();
	}
);

/**
 * Add necessary CSS and JS scripts.
 */
add_action(
	'admin_enqueue_scripts',
	function () {
		wp_enqueue_style( 'gm-academic-css', plugins_url( 'css/gm-academic.css', __FILE__ ), false, GM_ACADEMIC_VERSION );
		wp_enqueue_script( 'gm-academic-js', plugins_url( 'js/gm-academic.js', __FILE__ ), array( 'jquery' ), GM_ACADEMIC_VERSION, true );
	}
);

/**
 * Check if plugin DB schema needs to be updated.
 */
add_action(
	'plugins_loaded',
	function () {
		global $gm_academic_db_version;
		if ( intval( get_option( GM_ACADEMIC_DB_VERSION, '0' ) ) < $gm_academic_db_version ) {
			gm_academic_db_setup();
		}
	}
);

/**
 * Sets up the plugin database.
 */
function gm_academic_db_setup() {
	gm_academic_pid_db_setup();
}

/**
 * Returns entry author information
 *
 * @return string
 */
function gm_academic_get_author_info() {
	if ( function_exists( 'coauthors' ) ) {
		return coauthors( ', ', __( ' and ', 'gm-academic' ), null, null, false );
	} else {
		return get_the_author();
	}
}

/**
 * Add plugin settings.
 */
require plugin_dir_path( __FILE__ ) . 'inc/settings.php';

/**
 * Add support for changing the author base.
 */
require plugin_dir_path( __FILE__ ) . 'inc/rewrite-author-base.php';

/**
 * Add a new post type "article".
 */
require plugin_dir_path( __FILE__ ) . 'inc/custom-post-type.php';

/**
 * Support for displaying content licenses.
 */
require plugin_dir_path( __FILE__ ) . 'inc/license/license.php';

/**
 * Support for citation guide.
 */
require plugin_dir_path( __FILE__ ) . 'inc/citation-guide.php';

/**
 * Support for persistent identifiers.
 */
require plugin_dir_path( __FILE__ ) . 'inc/persistent-identifiers/persistent-identifiers.php';
