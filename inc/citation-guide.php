<?php
/**
 * Support for citation guide.
 *
 * @package GM_Academic
 */

/**
 * Renders the citation guide for the current post.
 */
function gm_academic_render_citation_guide() {
	global $post;

	$title      = get_the_title( $post );
	$categories = get_the_category( $post );
	$year       = get_the_date( 'F Y', $post );
	$authors    = gm_academic_get_author_info();

	$has_pid = false;
	if ( gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_PID ) && gm_academic_pid_is_active() ) {
		$has_pid = gm_academic_has_pid( $post->ID );
	}

	if ( ! $has_pid ) {
		$permalink         = get_the_permalink( $post );
		$permalink_display = wp_parse_url( $permalink, PHP_URL_HOST );
	} else {
		$pid               = gm_academic_get_current_pid_from_post( $post->ID );
		$permalink         = gm_academic_pid_get_link( $pid );
		$permalink_display = gm_academic_pid_to_string( $pid );
	}

	if ( ! empty( $categories ) ) {
		$category = $categories[0]->name;
	} else {
		$category = null;
	}

	$link_and_date = esc_html__( 'Online:', 'gm-academic' ) . ' <a href="' . esc_url_raw( $permalink ) . '"';
	if ( $has_pid ) {
		$link_and_date .= 'itemprop="sameAs"';
	}
	$link_and_date .= '>' . esc_html( $permalink_display ) . '</a>';
	if ( ! $has_pid ) {
		$link_and_date .= ', <span class="retrieval-date">';
		/* translators: %s is the current date */
		$link_and_date .= esc_html( sprintf( __( 'retrieved at %s', 'gm-academic' ), date_i18n( get_option( 'date_format' ), time() ) ) );
		$link_and_date .= '</span>';
	}

	$link_and_date .= '.';

	?>
	<div class="gm-academic-citation-guide">
		<h2 class="citation-guide-heading"><?php echo esc_html__( 'Cite as', 'gm-academic' ); ?></h2>
		<div class="citation-guide-content">
			<p class="citation-guide-intro"><?php echo esc_html__( 'Please cite this entry as follows.', 'gm-academic' ); ?></p>
			<p>
				<span class="authors"><?php echo esc_html( $authors ); ?></span>:
				<span class="title"><?php echo esc_html( $title ); ?>.</span>
				<?php echo esc_html__( 'In:', 'gm-academic' ); ?>
				<?php if ( null !== $category ) : ?>
					<span class="publication"><?php echo esc_html( $category ); ?></span>,
				<?php endif; ?>
				<span class="publisher"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></span>,
				<span class="year"><?php echo esc_html( $year ); ?></span>.
				<?php echo $link_and_date; // phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped ?>
			</p>
		</div>
	</div>
	<?php
}
