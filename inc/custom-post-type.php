<?php
/**
 * Add a custom post type "article".
 *
 * @package GM_Academic
 */

// constant for the name of the custom post type
define( 'GM_ACADEMIC_CUSTOM_POST_ARTICLE', 'gm_academic_article' );

// constant for the custom post type rewrite base
define( 'GM_ACADEMIC_ARTICLES_REWRITE_BASE', 'gm_academic_articles_base' );

if ( gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_ARTICLE ) ) {
	add_action( 'init', 'gm_academic_articles_register' );
	add_action( 'pre_get_posts', 'gm_academic_articles_query' );
	add_action( 'admin_init', 'gm_academic_articles_rewrite_settings' );

	add_filter( 'coauthors_supported_post_types', 'gm_academic_coauthors_plus_post_types' );
	add_filter( 'coauthors_count_published_post_types', 'gm_academic_coauthors_plus_post_types' );

	add_filter( 'ppl_post_types', 'gm_academic_articles_ppl_support' );
	add_filter( 'ppl_query_vars', 'gm_academic_articles_ppl_query' );
}

/**
 * Add custom post type.
 */
function gm_academic_articles_register() {
	register_post_type(
		GM_ACADEMIC_CUSTOM_POST_ARTICLE,
		array(
			'labels'        => array(
				'name'               => __( 'Articles', 'gm-academic' ),
				'singular_name'      => __( 'Article', 'gm-academic' ),
				'new_item'           => __( 'New article', 'gm-academic' ),
				'view_item'          => __( 'View article', 'gm-academic' ),
				'not_found'          => __( 'No articles found', 'gm-academic' ),
				'not_found_in_trash' => __( 'No articles found in trash', 'gm-academic' ),
				'all_items'          => __( 'All articles', 'gm-academic' ),
				'insert_into_item'   => __( 'Insert into article', 'gm-academic' ),
			),
			'public'        => true,
			'show_in_menu'  => true,
			'menu_position' => 5,
			'has_archive'   => true,
			'rewrite'       => array(
				'slug'       => ! empty( get_option( 'gm_academic_articles_base' ) ) ? get_option( 'gm_academic_articles_base' ) : 'articles',
				'with_front' => false,
			),
			'show_in_rest'  => true,
			'supports'      => array( 'title', 'editor', 'custom-fields', 'excerpt', 'thumbnail', 'revisions' ),
			'taxonomies'    => array( 'category', 'post_tag' ),
		)
	);
}

/**
 * Add custom post type to main query so that it shows up on archives and the like.
 *
 * @param WP_Query $query the main query.
 */
function gm_academic_articles_query( $query ) {
	if ( ! is_admin()
		&& ! $query->is_page()
		&& empty( $query->query_vars['suppress_filters'] )
		&& empty( $query->get( 'post_type' ) ) ) {
		$query->set(
			'post_type',
			array( 'post', 'gm_academic_article' )
		);
	}
	return $query;
}

/**
 * Add settings field for changing rewrite base.
 */
function gm_academic_articles_rewrite_settings() {
	add_settings_field(
		GM_ACADEMIC_ARTICLES_REWRITE_BASE,
		__( 'Articles base', 'gm-academic' ),
		'gm_academic_articles_base_output',
		'permalink',
		'optional'
	);

	$articles_base_db = get_option( GM_ACADEMIC_ARTICLES_REWRITE_BASE );

	if ( isset( $_POST[ GM_ACADEMIC_ARTICLES_REWRITE_BASE ] ) &&
		isset( $_POST['permalink_structure'] ) &&
		check_admin_referer( 'update-permalink' )
	) {
		$articles_base = sanitize_title( wp_unslash( $_POST[ GM_ACADEMIC_ARTICLES_REWRITE_BASE ] ) );

		if ( empty( $articles_base ) ) {
			add_settings_error(
				GM_ACADEMIC_ARTICLES_REWRITE_BASE,
				GM_ACADEMIC_ARTICLES_REWRITE_BASE,
				esc_html__( 'Invalid Articles Base.', 'gm-academic' ),
				'error'
			);
		} elseif ( $articles_base_db !== $articles_base ) {
			update_option( GM_ACADEMIC_ARTICLES_REWRITE_BASE, $articles_base );
		}
	}
}

/**
 * Outputs the input field for changing the rewrite base.
 */
function gm_academic_articles_base_output() {
	?>
	<input name="<?php echo esc_attr( GM_ACADEMIC_ARTICLES_REWRITE_BASE ); ?>" type="text" class="regular-text code" value="<?php echo esc_attr( get_option( GM_ACADEMIC_ARTICLES_REWRITE_BASE ) ); ?>" placeholder="<?php echo 'articles'; ?>" />
	<?php
}

/**
 * Add support for this post type to the coauthors plus plugin.
 *
 * @param array $post_types the post types array.
 */
function gm_academic_coauthors_plus_post_types( $post_types ) {
	$post_types[] = GM_ACADEMIC_CUSTOM_POST_ARTICLE;
	return $post_types;
}

/**
 * Add support for this post type to the per post language plugin.
 *
 * @param array $types supported post types.
 */
function gm_academic_articles_ppl_support( $types ) {
	if ( ! array_key_exists( GM_ACADEMIC_CUSTOM_POST_ARTICLE, $types ) ) {
		$post_type            = new stdClass();
		$post_type->query_var = GM_ACADEMIC_CUSTOM_POST_ARTICLE;

		$types[ GM_ACADEMIC_CUSTOM_POST_ARTICLE ] = $post_type;
	}
	return $types;
}

/**
 * Add query var for this post type for the per post language plugin.
 *
 * @param array $query_vars existing query vars.
 */
function gm_academic_articles_ppl_query( $query_vars ) {
	if ( ! array_key_exists( GM_ACADEMIC_CUSTOM_POST_ARTICLE, array_flip( $query_vars ) ) ) {
		$query_vars[] = GM_ACADEMIC_CUSTOM_POST_ARTICLE;
	}
	return $query_vars;
}
