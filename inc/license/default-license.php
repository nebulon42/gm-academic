<?php
/**
 * Add support for the default license.
 *
 * @package GM_Academic
 */

/**
 * Add a setting field for default license to the Reading Page.
 */
function gm_academic_license_settings() {
	if ( gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_LICENSES ) ) {
		add_settings_field(
			GM_ACADEMIC_LICENSE_DEFAULT_SETTING,
			esc_html__( 'Default License', 'gm-academic' ),
			'gm_academic_render_license_chooser_settings',
			'reading',
			'default',
			array( 'label_for' => GM_ACADEMIC_LICENSE_DEFAULT_SETTING )
		);
	}

	register_setting(
		'reading',
		GM_ACADEMIC_LICENSE_DEFAULT_SETTING,
		array(
			'type'              => 'string',
			'description'       => 'site default content license',
			'sanitize_callback' => 'sanitize_title',
			'show_in_rest'      => true,
			'default'           => 'c',
		)
	);
}
add_action( 'admin_init', 'gm_academic_license_settings' );

/**
 * Renders a license chooser in settings.
 */
function gm_academic_render_license_chooser_settings() {
	$license_value = gm_academic_get_default_license();
	?>
	<p id="gm_academic-license-intro"><?php echo esc_html__( 'Select one of the licenses below as default license for posts.', 'gm-academic' ); ?></p>
	<?php
	echo gm_academic_get_license_chooser( GM_ACADEMIC_LICENSE_DEFAULT_SETTING, $license_value ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

/**
 * Shortcode for default license statement.
 */
function gm_academic_default_license_shortcode() {
	if ( ! gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_LICENSES ) ) {
		return '';
	}

	$license_value = gm_academic_get_default_license();

	$license_url   = gm_academic_get_license_info_url( $license_value );
	$license_title = gm_academic_get_license_title( $license_value );
	$license_parts = array(
		gm_academic_get_license_images( $license_value ) . ' ',
	);

	if ( $license_url ) {
		$license_parts[] = '<a href="' . esc_url( $license_url ) . '">';
		$license_parts[] = $license_title;
		$license_parts[] = '</a>';
	} else {
		$license_parts[] = $license_title;
	}

	$license = implode( '', $license_parts );

	return '<span class="gm-academic-default-license">'
		// translators: %s is the rendered license information with title and images and links to the license details
		. sprintf( __( 'Unless otherwise noted the content is avaliable under the license %s.', 'gm-academic' ), $license )
		. '</span>';
}
add_shortcode( 'default-license', 'gm_academic_default_license_shortcode', 0, 10 );
