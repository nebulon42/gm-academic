<?php
/**
 * Add support for content licenses.
 *
 * @package GM_Academic
 */

// constant for the custom field name
define( 'GM_ACADEMIC_LICENSE_CUSTOM_FIELD', 'gm_academic_license' );

// constant for the name of form field for posts
define( 'GM_ACADEMIC_LICENSE_NAME_POST', 'gm_academic_license_post' );

// constant for the name of form field for media
define( 'GM_ACADEMIC_LICENSE_NAME_MEDIA', 'gm_academic_license_media' );

// constant for the name of original author field for media
define( 'GM_ACADEMIC_ORIGINAL_AUTHOR_MEDIA', 'gm_academic_original_author_media' );

// constant for the name of original work link field for media
define( 'GM_ACADEMIC_ORIGINAL_LINK_MEDIA', 'gm_academic_original_link_media' );

// constant for default license setting name
define( 'GM_ACADEMIC_LICENSE_DEFAULT_SETTING', 'gm_academic_license_default' );

// constant for supported licenses
define( 'GM_ACADEMIC_SUPPORTED_LICENSES', array( 'c', 'cc-by-nc-nd', 'cc-by-nd', 'cc-by-nc-sa', 'cc-by-nc', 'cc-by-sa', 'cc-by', 'cc-zero' ) );

/**
 * License template tags.
 */
require plugin_dir_path( __FILE__ ) . 'template-tags.php';

/**
 * Default license setting.
 */
require plugin_dir_path( __FILE__ ) . 'default-license.php';

/**
 * Support for post license.
 */
require plugin_dir_path( __FILE__ ) . 'post-license.php';

/**
 * Support for media license.
 */
require plugin_dir_path( __FILE__ ) . 'media-license.php';

/**
 * Returns the translated title of a license.
 *
 * @param string $identifier license identifier.
 */
function gm_academic_get_license_title( $identifier ) {
	switch ( $identifier ) {
		case 'c':
			$title = __( 'All rights reserved', 'gm-academic' );
			break;
		case 'cc-by-nc-nd':
			$title = __( 'Creative Commons Attribution-NonCommercial-NoDerivatives', 'gm-academic' );
			break;
		case 'cc-by-nd':
			$title = __( 'Creative Commons Attribution-NoDerivatives', 'gm-academic' );
			break;
		case 'cc-by-nc-sa':
			$title = __( 'Creative Commons Attribution-NonCommerical-ShareAlike', 'gm-academic' );
			break;
		case 'cc-by-nc':
			$title = __( 'Creative Commons Attribution-NonCommerical', 'gm-academic' );
			break;
		case 'cc-by-sa':
			$title = __( 'Creative Commons Attribution-ShareAlike', 'gm-academic' );
			break;
		case 'cc-by':
			$title = __( 'Creative Commons Attribution', 'gm-academic' );
			break;
		case 'cc-zero':
			$title = __( 'Creative Commons Zero/Public Domain dedication', 'gm-academic' );
			break;
		default:
			$title = '';
	}
	return $title;
}

/**
 * Returns the translated abbreviation of a license.
 *
 * @param string $identifier license identifier.
 */
function gm_academic_get_license_abbr( $identifier ) {
	switch ( $identifier ) {
		case 'c':
			$title = __( 'All rights reserved', 'gm-academic' );
			break;
		case 'cc-by-nc-nd':
			$title = __( 'CC-BY-NC-ND', 'gm-academic' );
			break;
		case 'cc-by-nd':
			$title = __( 'CC-BY-ND', 'gm-academic' );
			break;
		case 'cc-by-nc-sa':
			$title = __( 'CC-BY-NC-SA', 'gm-academic' );
			break;
		case 'cc-by-nc':
			$title = __( 'CC-BY-NC', 'gm-academic' );
			break;
		case 'cc-by-sa':
			$title = __( 'CC-BY-SA', 'gm-academic' );
			break;
		case 'cc-by':
			$title = __( 'CC-BY', 'gm-academic' );
			break;
		case 'cc-zero':
			$title = __( 'CC0', 'gm-academic' );
			break;
		default:
			$title = '';
	}
	return $title;
}

/**
 * Returns the translated internal description of a license.
 *
 * @param string $identifier license identifier.
 */
function gm_academic_get_license_description_internal( $identifier ) {
	switch ( $identifier ) {
		case 'c':
			$description = __( 'Reuse and re-publishing are prohibited. Not a Free Culture license.', 'gm-academic' );
			break;
		case 'cc-by-nc-nd':
			$description = __( 'Sharing is possible with attribution for non-commercial purposes if the work is not changed. Not a Free Culture license.', 'gm-academic' );
			break;
		case 'cc-by-nd':
			$description = __( 'Sharing is possible with attribution if the work is not changed. Not a Free Culture license.', 'gm-academic' );
			break;
		case 'cc-by-nc-sa':
			$description = __( 'Sharing is possible with attribution for non-commercial purposes if the derivate work is published under the same license. Not a Free Culture license.', 'gm-academic' );
			break;
		case 'cc-by-nc':
			$description = __( 'Sharing is possible with attribution for non-commercial purposes. Not a Free Culture license.', 'gm-academic' );
			break;
		case 'cc-by-sa':
			$description = __( 'Sharing is possible with attribution if the derivative work is published under the same license. <strong>A Free Culture license.</strong>', 'gm-academic' );
			break;
		case 'cc-by':
			$description = __( 'Sharing is possible with attribution. <strong>A Free Culture license.</strong>', 'gm-academic' );
			break;
		case 'cc-zero':
			$description = __( 'Sharing is possible with as little restrictions as possible by law. <strong>A Free Culture license.</strong>', 'gm-academic' );
			break;
		default:
			$description = '';
	}
	return $description;
}

/**
 * Returns markup of a license chooser.
 *
 * @param string $field_name name of the form field.
 * @param string $license_value the current license value.
 * @return string
 */
function gm_academic_get_license_chooser( $field_name, $license_value ) {
	$output = '<ul id="gm-academic-license-selection">' . "\n";
	foreach ( GM_ACADEMIC_SUPPORTED_LICENSES as $license ) {
		$output .= '<li class="license-selector">' . "\n";
		$output .= gm_academic_get_license_meta_radio( $field_name, $license, $license_value ) . "\n";
		$output .= '<label for="' . $license . '">' . "\n";
		$output .= gm_academic_get_license_images( $license, 15 ) . "\n";
		$output .= '<strong>' . gm_academic_get_license_title( $license ) . '</strong>' . "\n";
		$output .= gm_academic_get_license_info_link( $license ) . "\n";
		$output .= '<br>' . gm_academic_get_license_description_internal( $license ) . "\n";
		$output .= '</label>' . "\n";
		$output .= '</li>' . "\n";
	}
	$output .= '</ul>' . "\n";
	$output .= '<p id="gm_academic-license-info">' . __( 'Find out what is meant with <a href="https://freedomdefined.org/Definition" target="_blank">Free Cultural Works</a>.', 'gm-academic' ) . '</p>' . "\n";
	return $output;
}

/**
 * Returns a license meta box radio button.
 *
 * @param string $field_name name of the form field.
 * @param string $identifier id extension and value of the radio button.
 * @param string $value value of the selected license.
 * @return string
 */
function gm_academic_get_license_meta_radio( $field_name, $identifier, $value ) {
	if ( ! in_array( $identifier, GM_ACADEMIC_SUPPORTED_LICENSES, true ) ) {
		return;
	}

	$output = '<input type="radio" id="gm_academic-license-' . $identifier . '" name="' . $field_name . '" value="' . $identifier . '"';
	if ( $identifier === $value ) {
		$output .= ' checked';
	}
	$output .= '>';
	return $output;
}

/**
 * Returns a image representation of a license.
 *
 * @param string $identifier dash separated identifier of the license e.g. cc-by-sa.
 * @param int    $size the size of the images.
 * @param string $container the name of the container tag.
 * @return string
 */
function gm_academic_get_license_images( $identifier, $size = 20, $container = 'span' ) {
	if ( ! in_array( $identifier, GM_ACADEMIC_SUPPORTED_LICENSES, true ) ) {
		return;
	}

	$parts  = explode( '-', $identifier );
	$output = '<' . $container . ' class="license-images">';
	foreach ( $parts as $part ) {
		$path = GM_ACADEMIC_BASE_PATH . 'img/' . $part . '.svg';
		if ( file_exists( $path ) ) {
			$url     = esc_url_raw( GM_ACADEMIC_BASE_URL . 'img/' . $part . '.svg' );
			$output .= '<img src="' . $url . '" alt="' . $part . '" width="' . $size . '" height="' . $size . '">';
		}
	}
	$output .= '</' . $container . '>';
	return $output;
}

/**
 * Returns a URL with further info about the license.
 *
 * @param string $identifier the license identifier.
 */
function gm_academic_get_license_info_url( $identifier ) {
	if ( 'c' === $identifier ||
		! in_array( $identifier, GM_ACADEMIC_SUPPORTED_LICENSES, true ) ) {
		return null;
	}

	if ( 'cc-zero' !== $identifier ) {
		return 'https://creativecommons.org/licenses/' . substr( $identifier, 3 ) . '/4.0/deed.' . substr( get_locale(), 0, 2 );
	} else {
		return 'https://creativecommons.org/publicdomain/' . substr( $identifier, 3 ) . '/1.0/deed.' . substr( get_locale(), 0, 2 );
	}
}

/**
 * Returns a link with more information for a license identifier.
 *
 * @param string $identifier the license identifier.
 * @return string
 */
function gm_academic_get_license_info_link( $identifier ) {
	$allowed_identifiers = GM_ACADEMIC_SUPPORTED_LICENSES;
	$pos                 = array_search( 'c', $allowed_identifiers, true );
	if ( false !== $pos ) {
		unset( $allowed_identifiers[ $pos ] );
	}
	if ( in_array( $identifier, $allowed_identifiers, true ) ) {
		return ' <a href="' . esc_url_raw( gm_academic_get_license_info_url( $identifier ) ) . '" target="_blank">' . __( 'More info', 'gm-academic' ) . '</a>';
	}
	return '';
}
