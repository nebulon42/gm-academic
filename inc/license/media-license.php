<?php
/**
 * Add support for media licenses.
 *
 * @package GM_Academic
 */

if ( gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_LICENSES ) ) {
	add_filter( 'attachment_fields_to_edit', 'gm_academic_license_media_settings', 10, 2 );
	add_filter( 'attachment_fields_to_save', 'gm_academic_license_media_save', 10, 2 );

	add_filter( 'render_block_data', 'gm_academic_add_attribution_to_image_block', 10, 1 );
}

/**
 * Whether the post has media that have a different license.
 *
 * @param object $post post object.
 * @return bool
 */
function gm_academic_has_differently_licensed_media( $post ) {
	$content      = $post->post_content;
	$post_license = gm_academic_get_post_license( $post );

	// check featured image
	$featured_img_id = get_post_thumbnail_id( $post );
	if ( false !== $featured_img_id && 0 < $featured_img_id ) {
		$media_license = gm_academic_get_post_license( $featured_img_id );
		if ( $post_license !== $media_license ) {
			return true;
		}
	}

	$matches = array();
	// extract all ids of images in this post (Gutenberg blocks only)
	preg_match_all( '/<\!\-\- wp\:image \{.*"id":([0-9]+).*} \-\->/', $content, $matches, PREG_PATTERN_ORDER );
	if ( isset( $matches[1] ) && is_array( $matches[1] ) ) {
		foreach ( $matches[1] as $id ) {
			$media_license = gm_academic_get_post_license( $id );
			if ( $post_license !== $media_license ) {
				return true;
			}
		}
	}
	return false;
}

/**
 * Renders a license chooser for a post.
 *
 * @param object $post the post object.
 * @return string
 */
function gm_academic_get_license_chooser_media( $post ) {
	$license_value = gm_academic_get_post_license( $post );
	$output        = '<p id="gm_academic-license-intro">' . esc_html__( 'Select one of the licenses below for your media.', 'gm-academic' ) . '</p>';
	$output       .= gm_academic_get_license_chooser( 'attachments[' . $post->ID . '][' . GM_ACADEMIC_LICENSE_NAME_MEDIA . ']', $license_value );
	return $output;
}

/**
 * Specify custom fields for media.
 *
 * @param array  $form_fields the form fields.
 * @param object $post the post.
 */
function gm_academic_license_media_settings( $form_fields, $post ) {
	$form_fields[ GM_ACADEMIC_ORIGINAL_AUTHOR_MEDIA ] = array(
		'label' => __( 'Original Author', 'gm-academic' ),
		'input' => 'text',
		'value' => get_post_meta( $post->ID, GM_ACADEMIC_ORIGINAL_AUTHOR_MEDIA, true ),
		'helps' => __( 'Specifiy the original author of the work if existing.', 'gm-academic' ),
	);

	$form_fields[ GM_ACADEMIC_ORIGINAL_LINK_MEDIA ] = array(
		'label' => __( 'Link to Original Work', 'gm-academic' ),
		'input' => 'text',
		'value' => get_post_meta( $post->ID, GM_ACADEMIC_ORIGINAL_LINK_MEDIA, true ),
		'helps' => __( 'Specify the link to the original work if existing.', 'gm-academic' ),
	);

	$form_fields[ GM_ACADEMIC_LICENSE_NAME_MEDIA ] = array(
		'label' => __( 'Media License', 'gm-academic' ),
		'input' => 'html',
		'html'  => gm_academic_get_license_chooser_media( $post ),
	);
	return $form_fields;
}

/**
 * Saving the custom media values.
 *
 * @param object $post the post.
 * @param array  $attachment the attchment.
 */
function gm_academic_license_media_save( $post, $attachment ) {
	$id = isset( $post['post_ID'] ) ? $post['post_ID'] : ( isset( $post['ID'] ) ? $post['ID'] : null );
	if ( $id ) {
		// processing license field
		if ( isset( $attachment[ GM_ACADEMIC_LICENSE_NAME_MEDIA ] ) ) {
			$value = sanitize_title( $attachment[ GM_ACADEMIC_LICENSE_NAME_MEDIA ] );
			if ( $value ) {
				$current_value = get_post_meta( $id, GM_ACADEMIC_LICENSE_CUSTOM_FIELD );
				if ( $current_value !== $value ) {
					update_post_meta( $id, GM_ACADEMIC_LICENSE_CUSTOM_FIELD, $value );
				}
			}
		}

		// processing original author field
		if ( isset( $attachment[ GM_ACADEMIC_ORIGINAL_AUTHOR_MEDIA ] ) ) {
			$value = sanitize_text_field( $attachment[ GM_ACADEMIC_ORIGINAL_AUTHOR_MEDIA ] );
			if ( $value ) {
				update_post_meta( $id, GM_ACADEMIC_ORIGINAL_AUTHOR_MEDIA, $value );
			}
		}

		// processing original link field
		if ( isset( $attachment[ GM_ACADEMIC_ORIGINAL_LINK_MEDIA ] ) ) {
			$value = esc_url_raw( $attachment[ GM_ACADEMIC_ORIGINAL_LINK_MEDIA ] );
			if ( $value ) {
				update_post_meta( $id, GM_ACADEMIC_ORIGINAL_LINK_MEDIA, $value );
			}
		}
	}

	return $post;
}

/**
 * Add attribution to image.
 *
 * @param array $block the block data.
 */
function gm_academic_add_attribution( $block ) {
	if ( isset( $block['attrs']['id'] ) ) {
		libxml_use_internal_errors( true );
		$block_dom = new \DomDocument();
		$block_dom->loadHTML( '<?xml encoding="utf-8"?>' . $block['innerContent'][0], LIBXML_HTML_NODEFDTD );

		$image = $block_dom->getElementsByTagName( 'img' );
		if ( $image->count() !== 0 ) {
			$image = $image->item( 0 );
			$image->setAttribute( 'itemprop', 'contentUrl' );

			$figure = $block_dom->getElementsByTagName( 'figure' );
			if ( $figure->count() !== 0 ) {
				$figure = $figure->item( 0 );
				$figure->setAttribute( 'itemscope', '' );
				$figure->setAttribute( 'itemtype', 'https://schema.org/ImageObject' );

				$attribution = gm_academic_get_media_attribution( $block['attrs']['id'] );
				// if we have attribution
				if ( strlen( $attribution ) > 0 ) {
					// add attribution class to figure
					$classes   = explode( ' ', $figure->getAttribute( 'class' ) );
					$classes[] = 'gm-academic-has-media-attribution';
					$figure->setAttribute( 'class', implode( ' ', $classes ) );

					$image_width = null;
					// try to get image width from width tag
					if ( $image->hasAttribute( 'width' ) ) {
						$image_width = $image->getAttribute( 'width' );
					} else {
						// if there was no width tag try to guess width from file name
						$img_file = wp_parse_url( $image->getAttribute( 'src' ), PHP_URL_PATH );
						if ( $img_file ) {
							// we now have extracted the file name of the img from the URL
							$img_file = basename( $img_file );
							// we now search for e.g. -156x300. parts that denote width and height
							$matches = array();
							if ( preg_match( '/\-([0-9]+)x[0-9]+\./', $img_file, $matches ) ) {
								if ( isset( $matches[1] ) ) {
									$image_width = $matches[1];
								}
							}
						}
					}

					$container = $block_dom->createElement( 'div' );
					if ( $image_width ) {
						$container_width        = $block_dom->createAttribute( 'style' );
						$container_width->value = 'max-width: ' . $image_width . 'px;';
						$container->appendChild( $container_width );
					}

					$image->parentNode->replaceChild( $container, $image ); // phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
					$container->appendChild( $image );

					$attribution_dom = new \DomDocument();
					$attribution_dom->loadHTML( '<?xml encoding="utf-8"?>' . $attribution, LIBXML_HTML_NODEFDTD );
					$attribution_child   = $attribution_dom->documentElement->firstChild; // phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
					$attribution_element = $block_dom->importNode( $attribution_child, true );

					$container->appendChild( $attribution_element );

					$new_content = $block_dom->saveHTML();
					if ( false !== $new_content ) {
						$new_content = str_replace( '<?xml encoding="utf-8"?>', '', $new_content );
						$new_content = str_replace( '<html>', '', $new_content );
						$new_content = str_replace( '</html>', '', $new_content );
						$new_content = str_replace( '<body>', '', $new_content );
						$new_content = str_replace( '</body>', '', $new_content );

						$block['innerContent'][0] = $new_content;
					}
				}
			}
		}
	}
	return $block;
}

/**
 * Hooking into the block rendering process to add license information to image blocks.
 *
 * @param array $block the block data.
 */
function gm_academic_add_attribution_to_image_block( $block ) {
	// we only manipulate the core/image blocks
	if ( empty( $block['innerBlocks'] ) && 'core/image' === $block['blockName'] ) {
		$block = gm_academic_add_attribution( $block );
	} elseif ( ! empty( $block['innerBlocks'] ) ) {
		$inner_blocks_count = count( $block['innerBlocks'] );
		for ( $i = 0; $i < $inner_blocks_count; $i++ ) {
			$block['innerBlocks'][ $i ] = gm_academic_add_attribution_to_image_block( $block['innerBlocks'][ $i ] );
		}
	}

	return $block;
}
