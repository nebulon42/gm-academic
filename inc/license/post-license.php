<?php
/**
 * Add support for post license.
 *
 * @package GM_Academic
 */

if ( gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_LICENSES ) ) {
	add_action( 'add_meta_boxes', 'gm_academic_post_licence_meta' );
	add_action( 'save_post', 'gm_academic_post_licence_save' );
}

/**
 * Registering the license meta box.
 */
function gm_academic_post_licence_meta() {
	$post_types = array( 'post', GM_ACADEMIC_CUSTOM_POST_ARTICLE );

	add_meta_box(
		'gm_academic_license',
		__( 'License', 'gm-academic' ),
		'gm_academic_render_license_chooser_post',
		apply_filters( 'gm_academic_license_meta_box_post_types', $post_types ),
		apply_filters( 'gm_academic_license_meta_box_context', 'normal' ),
		apply_filters( 'gm_academic_license_meta_box_priority', 'high' )
	);
}

/**
 * Renders a license chooser for a post.
 *
 * @param object $post the post object.
 */
function gm_academic_render_license_chooser_post( $post ) {
	$license_value = gm_academic_get_post_license( $post );
	?>
	<p id="gm_academic-license-intro"><?php echo esc_html__( 'Select one of the licenses below for your post.', 'gm-academic' ); ?></p>
	<?php
	echo gm_academic_get_license_chooser( GM_ACADEMIC_LICENSE_NAME_POST, $license_value ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	wp_nonce_field( 'gm_academic-license-edit_' . $post->ID, 'gm_academic-nonce' );
}

/**
 * Saving the license value.
 */
function gm_academic_post_licence_save() {
	global $post;

	if ( ! isset( $_REQUEST['gm_academic-nonce'] ) || ! wp_verify_nonce( sanitize_key( wp_unslash( $_REQUEST['gm_academic-nonce'] ) ), 'gm_academic-license-edit_' . $post->ID ) ) {
		return;
	}

	$value = isset( $_POST[ GM_ACADEMIC_LICENSE_NAME_POST ] ) ? sanitize_title( wp_unslash( $_POST[ GM_ACADEMIC_LICENSE_NAME_POST ] ) ) : null;
	if ( $value ) {
		$current_value = get_post_meta( $post->ID, GM_ACADEMIC_LICENSE_CUSTOM_FIELD );
		if ( $current_value !== $value ) {
			update_post_meta( $post->ID, GM_ACADEMIC_LICENSE_CUSTOM_FIELD, $value );
		}
	}
}
