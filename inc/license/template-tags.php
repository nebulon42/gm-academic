<?php
/**
 * Template tags for content licenses.
 *
 * @package GM_Academic
 */

/**
 * Returns the value of the post license.
 *
 * @param object|int|null $post_param the post.
 * @return string|null the set license or the default license if unset for the specific post.
 */
function gm_academic_get_post_license( $post_param = null ) {
	global $post;

	if ( ! gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_LICENSES ) ) {
		return null;
	}

	$post_id = null;
	if ( null === $post_param ) {
		$post_id = $post->ID;
	} elseif ( $post_param instanceof WP_Post ) {
		$post_id = $post_param->ID;
	} elseif ( is_numeric( $post_param ) ) {
		$post_id = $post_param;
	}

	if ( $post_id ) {
		$value = get_post_meta( $post_id, GM_ACADEMIC_LICENSE_CUSTOM_FIELD, true );
	} else {
		$value = null;
	}
	return ( false === $value || strlen( $value ) === 0 ) ? gm_academic_get_default_license() : $value;
}

/**
 * Returns the default license for posts.
 *
 * @return string
 */
function gm_academic_get_default_license() {
	$value = get_option( GM_ACADEMIC_LICENSE_DEFAULT_SETTING, 'c' );
	// return all rights reserved in case the value is not set
	// this should not happen with the default defined, but did happen
	if ( ! $value ) {
		return 'c';
	}
	return $value;
}

/**
 * Renders the license for the current post.
 */
function gm_academic_render_post_license() {
	global $post;

	if ( ! gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_LICENSES ) ) {
		return;
	}

	$license = gm_academic_get_post_license();
	?>
	<div class="gm-academic-license-info">
		<h2 class="license-heading"><?php echo esc_html__( 'License', 'gm-academic' ); ?></h2>
		<div class="license-content">
			<p class="license-intro"><?php echo esc_html__( 'The content of this entry is available under the following license.', 'gm-academic' ); ?></p>
			<div class="license">
				<?php echo wp_kses( gm_academic_get_license_images( $license, 40, 'div' ), wp_kses_allowed_html( 'post' ) ); ?>
				<div class="license-title">
					<?php echo esc_html( gm_academic_get_license_title( $license ) ); ?>
				</div>
			</div>
			<?php if ( gm_academic_license_has_attribution( $license ) ) : ?>
			<div class="attribution-info">
				<?php
				$attribution = gm_academic_get_author_info();
				// translators: %s is the attribution information
				echo esc_html( sprintf( __( 'The license requires attribution in the form: „%s“', 'gm-academic' ), $attribution ) );
				?>
			</div>
			<?php endif; ?>
			<?php $url = gm_academic_get_license_info_url( $license ); ?>
			<?php if ( $url ) : ?>
			<p class="license-info">
				<?php
				echo wp_kses(
					// translators: %s is the link url
					sprintf( __( 'Get <a href="%s" itemprop="license">more information</a> about what is allowed and disallowed under this license.', 'gm-academic' ), esc_url( $url ) ),
					array(
						'a' => array(
							'href'     => array(),
							'target'   => array(),
							'itemprop' => array(),
						),
					)
				);
				?>
			</p>
			<?php endif; ?>
			<?php if ( gm_academic_has_differently_licensed_media( $post ) ) : ?>
			<p class="media-info">
				<?php echo esc_html__( 'Please note that this entry has media with different license. The license is listed at the medium.', 'gm-academic' ); ?>
			<?php endif; ?>
		</div>
	</div>
	<?php
}

/**
 * Returns whether the license requires attribution.
 *
 * @param string $identifier license identifier.
 * @return bool returns true when the license requires attribution, false otherwise. Also returns false if the license is not supported.
 */
function gm_academic_license_has_attribution( $identifier ) {
	if ( ! in_array( $identifier, GM_ACADEMIC_SUPPORTED_LICENSES, true ) ) {
		return false;
	}

	$parts = explode( '-', $identifier );
	if ( in_array( 'by', $parts, true ) ) {
		return true;
	}
	return false;
}

/**
 * Returns markup for media attribution.
 *
 * @param integer $post_id id of the post.
 * @return string the markup.
 */
function gm_academic_get_media_attribution( $post_id ) {
	global $post;

	$original_author_display = false;
	$license_display         = false;

	$content = '';
	if ( ! gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_LICENSES ) ) {
		return $content;
	}

	$original_author = get_post_meta( $post_id, GM_ACADEMIC_ORIGINAL_AUTHOR_MEDIA, true );
	$original_link   = get_post_meta( $post_id, GM_ACADEMIC_ORIGINAL_LINK_MEDIA, true );
	if ( $original_author ) {
		$content .= '<span class="gm-academic-media-author">';
		if ( $original_link ) {
			$content .= '<a href="' . esc_url_raw( $original_link ) . '" target="_blank" itemprop="url"><span itemprop="creditText">' . esc_html( $original_author ) . '</span></a>';
		} else {
			$content .= '<span itemprop="creditText">' . esc_html( $original_author ) . '</span>';
		}
		$content                .= '</span>';
		$original_author_display = true;
	}

	$media_license = gm_academic_get_post_license( $post_id );
	$post_license  = gm_academic_get_post_license( $post->ID );
	if ( $media_license !== $post_license ) {
		$content        .= '<span class="gm-academic-media-license">';
		$license_display = true;
	} else {
		$content .= '<span class="gm-academic-media-license semantic-only">';
	}

	if ( $original_author && ( 'c' !== $media_license || $media_license !== $post_license ) ) {
		$content .= apply_filters( 'gm_academic_media_attribution_separator', ' / ' );
	}
	if ( 'c' !== $media_license ) {
		$content .= '<a href="' . gm_academic_get_license_info_url( $media_license ) . '" target="_blank" itemprop="license">';
		$content .= '<abbr title="' . gm_academic_get_license_title( $media_license ) . '">' . gm_academic_get_license_abbr( $media_license ) . '</abbr>';
		$content .= '</a>';
	} elseif ( $media_license !== $post_license ) {
		$content .= gm_academic_get_license_title( $media_license );
	}
	$content .= '</span>';

	if ( $original_author_display || $license_display ) {
		$content = '<div class="gm-academic-media-attribution">' . $content . '</div>';
	} else {
		$content = '<div class="gm-academic-media-attribution semantic-only">' . $content . '</div>';
	}

	return $content;
}
