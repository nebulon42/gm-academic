<?php
/**
 * Minter class.
 *
 * @package GM_Academic
 */

// converted from https://github.com/ruby-microservices/noid/blob/fe92479eac94dd24d4ba67ddad88df26bb8b2223/lib/noid/minter.rb (MIT Licsene)

require plugin_dir_path( __FILE__ ) . 'class-template.php';

/**
 * Minters come in two varieties: stateful and stateless. A stateless minter --
 * typically used with random rather than sequential templates, since minting
 * in a sequence requires state to know the current position in the sequence --
 * mints random identifiers and **will** mint duplicates eventually, depending
 * upon the size of the identifier space in the provided template.
 *
 * A stateful minter is a minter that has been initialized with parameters
 * reflecting its current state. (A common way to store state between mintings
 * is to call the minter `dump` method which serializes the necessary parts of
 * minter state to a hash, which may be persisted on disk or in other
 * back-ends.) The parameters that are included are:
 *
 * * template, a string setting the identifier pattern
 * * counters, a hash of "buckets" each with a current and max value
 * * seq, an integer reflecting how far into the sequence the minter is
 * * rand, a random number generator
 *
 * Minters using random templates use a number of containers, each with a
 * similar number of identifiers to split the identifier space into manageable
 * chunks (or "buckets") and to increase the appearance of randomness in the
 * identifiers.
 *
 * As an example, let's assume a random identifier template that has 100
 * possible values. It might have 10 buckets, each with 10 identifiers that
 * look similar because they have similar numeric values. Every call to `#mint`
 * will use the random number generator stored in the minter's state to select
 * a bucket at random. Stateless minters will select a bucket at random as
 * well.
 *
 * The difference between stateless and stateful minters in this context is
 * that stateful random minters are *replayable* as long as you have persisted
 * the minter's state, which includes a random number generator part of which
 * is its original seed, which may be used over again in the future to replay
 * the sequence of identifiers in this minter
 */
class Minter {
	/**
	 * The minter template.
	 *
	 * @var Template $template
	 */
	protected $template;

	/**
	 * Value for max seq.
	 *
	 * @var mixed $seq
	 */
	protected $seq;

	/**
	 * Value for counters.
	 *
	 * @var mixed $counters
	 */
	protected $counters;

	/**
	 * Value for max counters.
	 *
	 * @var integer $max_counters
	 */
	protected $max_counters;

	/**
	 * Callback function for after minting.
	 *
	 * @var callable $after_mint
	 */
	protected $after_mint;

	/**
	 * The random value generator.
	 *
	 * @var Random\Randomizer $rand
	 */
	protected $rand = null;

	/**
	 * Default value for max counters.
	 *
	 * @var integer $max_counters_default
	 */
	protected static $max_counters_default = 293;

	/**
	 * Returns the template.
	 */
	public function get_template() {
		return $this->template;
	}

	/**
	 * Returns the sequence.
	 */
	public function get_seq() {
		return $this->seq;
	}

	/**
	 * Sets the counters.
	 *
	 * @param mixed $counters the value for counters.
	 */
	public function set_counters( $counters ) {
		$this->counters = $counters;
	}

	/**
	 * Initializes the minter.
	 *
	 * @param array $options the options.
	 */
	public function initialize( $options = array() ) {
		$template_options = array_key_exists( 'template', $options ) ? $options['template'] : array();
		$this->template   = new Template();
		$this->template->initialize( $template_options );

		$this->counters     = array_key_exists( 'counters', $options ) ? $options['counters'] : null;
		$this->max_counters = array_key_exists( 'max_counters', $options ) ? $options['max_counters'] : null;

		// callback when an identifier is minted
		if ( array_key_exists( 'after_mint', $options ) && is_callable( $options['after_mint'] ) ) {
			$this->after_mint = $options['after_mint'];
		} else {
			$this->after_mint = null;
		}

		// used for random minters
		if ( array_key_exists( 'rand', $options ) ) {
			if ( is_object( $options['rand'] ) && $options['rand'] instanceof Random\Randomizer ) {
				$this->rand = $options['rand'];
			} else {
				$this->rand = new Random\Randomizer( new Random\Engine\PcgOneseq128XslRr64() );
				$this->rand->__unserialize( $options['rand'] );
			}
		}

		if ( null === $this->rand ) {
			if ( array_key_exists( 'seed', $options ) ) {
				$this->rand = new Random\Randomizer( new Random\Engine\PcgOneseq128XslRr64( $options['seed'] ) );
			} else {
				$this->rand = new Random\Randomizer( new Random\Engine\PcgOneseq128XslRr64() );
			}
		}

		// used for sequential minters
		$this->seq = array_key_exists( 'seq', $options ) ? $options['seq'] : 0;
	}

	/**
	 * Mint a new identifier.
	 */
	public function mint() {
		$n  = $this->next_in_sequence();
		$id = $this->template->mint( $n );
		if ( $this->is_random() ) {
			$this->next_sequence();
		}

		if ( null !== $this->after_mint ) {
			( $this->after_mint )( $id );
		}

		return $id;
	}

	/**
	 * Reseed the RNG.
	 *
	 * @param mixed   $seed_number the seed.
	 * @param integer $sequence the number of random steps.
	 */
	public function seed( $seed_number, $sequence = 0 ) {
		$this->rand = new Random\Randomizer( new Random\Engine\PcgOneseq128XslRr64( $seed_number ) );
		for ( $i = 0; $i < $sequence; $i++ ) {
			$this->next_random();
		}
		return $this->rand;
	}

	/**
	 * Is the identifier valid under the template string and checksum?
	 *
	 * @param string $id the identifier.
	 * @return bool whether the identifier is valid
	 */
	public function is_valid( $id ) {
		return $this->template->is_valid( $id );
	}

	/**
	 * Returns the number of identifiers remaining in the minter.
	 *
	 * @return float
	 */
	public function remaining() {
		if ( $this->is_unbounded() ) {
			return INF;
		}
		return $this->template->max() - $this->seq;
	}

	/**
	 * Returns the next value in sequence.
	 */
	public function next_in_sequence() {
		if ( $this->is_random() ) {
			return $this->next_random();
		}
		return $this->next_sequence();
	}

	/**
	 * Returns the next random value.
	 *
	 * @throws Exception When the sequence pool is exhausted.
	 */
	public function next_random() {
		if ( 0 === count( $this->counters() ) ) {
			throw new Exception( 'Exhausted noid sequence pool' );
		}
		$i                              = $this->random_bucket();
		$n                              = $this->counters[ $i ]['value'];
		$this->counters[ $i ]['value'] += 1;
		if ( $this->counters[ $i ]['value'] >= $this->counters[ $i ]['max'] ) {
			unset( $this->counters[ $i ] );
			$this->counters = array_values( $this->counters ); // re-index because PHP would keep the idx gap
		}
		return $n;
	}

	/**
	 * Returns the next sequence.
	 */
	public function next_sequence() {
		$seq        = $this->seq;
		$this->seq += 1;
		return $seq;
	}

	/**
	 * Returns a random number.
	 */
	public function random_bucket() {
		$random_nr = $this->rand->getInt( 0, count( $this->counters ) - 1 );
		return $random_nr;
	}

	/**
	 * Counters to use for quasi-random NOID sequences
	 */
	public function counters() {
		if ( null !== $this->counters ) {
			return $this->counters;
		}

		if ( ! $this->is_random() ) {
			return array();
		}

		if ( null !== $this->max_counters ) {
			$max_counters = $this->max_counters;
		} else {
			$max_counters = self::$max_counters_default;
		}
		$percounter     = floor( $this->template->max() / $max_counters + 1 );
		$t              = 0;
		$this->counters = array();

		while ( $t < $this->template->max() ) {
			$counter = array(
				'value' => $t,
				'max'   => min( $t + $percounter, $this->template->max() ),
			);

			$t               += $percounter;
			$this->counters[] = $counter;
		}

		return $this->counters;
	}

	/**
	 * Dumps the state of the minter.
	 *
	 * @return array
	 */
	public function dump() {
		return array(
			'template' => $this->template->get_template(),
			'counters' => $this->counters,
			'seq'      => $this->seq,
			'rand'     => $this->rand->__serialize(),
		);
	}

	/**
	 * Whether the minter uses random numbers.
	 */
	public function is_random() {
		return 'r' === $this->template->get_generator();
	}

	/**
	 * Whether the minter uses a sequence.
	 */
	public function is_unbounded() {
		return 'z' === $this->template->get_generator();
	}
}
