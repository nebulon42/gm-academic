<?php
/**
 * Template class.
 *
 * @package GM_Academic
 */

// converted from https://github.com/ruby-microservices/noid/blob/fe92479eac94dd24d4ba67ddad88df26bb8b2223/lib/noid/template.rb (MIT License)

/**
 * Holds the NOID template and other logic.
 */
class Template {

	/**
	 * The template.
	 *
	 * @var string $template
	 */
	protected $template;

	/**
	 * The prefix.
	 *
	 * @var string $prefix
	 */
	protected $prefix;

	/**
	 * The generator.
	 *
	 * @var mixed $generator
	 */
	protected $generator;

	/**
	 * The characters.
	 *
	 * @var mixed $characters
	 */
	protected $characters;

	/**
	 * The checkdigit.
	 *
	 * @var char $checkdigit
	 */
	protected $checkdigit;

	/**
	 * The "extended digit" pattern.
	 *
	 * @var string $xdigit_pattern
	 */
	protected $xdigit_pattern = null;

	/**
	 * The size list.
	 *
	 * @var mixed $size_list
	 */
	protected $size_list = null;

	/**
	 * The minimum.
	 *
	 * @var integer $min
	 */
	protected $min;

	/**
	 * The maximum.
	 *
	 * @var integer $max
	 */
	protected $max;

	/**
	 * The template validation regex pattern.
	 *
	 * @var string $validation_regex
	 */
	protected $validation_regex = null;

	/**
	 * A valid template pattern.
	 *
	 * @var string $valid_pattern
	 */
	protected static $valid_pattern = '/\A(.*)\.([rsz])([ed]+)(k?)\Z/';

	/**
	 * Allowed "extended digits".
	 *
	 * @var array $xdigit
	 */
	protected static $xdigit = array(
		'0',
		'1',
		'2',
		'3',
		'4',
		'5',
		'6',
		'7',
		'8',
		'9',
		'b',
		'c',
		'd',
		'f',
		'g',
		'h',
		'j',
		'k',
		'm',
		'n',
		'p',
		'q',
		'r',
		's',
		't',
		'v',
		'w',
		'x',
		'z',
	);

	/**
	 * Returns the template.
	 */
	public function get_template() {
		return $this->template;
	}

	/**
	 * Returns the prefix.
	 */
	public function get_prefix() {
		return $this->prefix;
	}

	/**
	 * Returns the generator.
	 */
	public function get_generator() {
		return $this->generator;
	}

	/**
	 * Returns the characters.
	 */
	public function get_characters() {
		return $this->characters;
	}

	/**
	 * Initializes the template.
	 *
	 * @param string/Template $template a coded string of the form "Prefix.Mask" governing how identifiers will be minted.
	 */
	public function initialize( $template ) {
		if ( is_string( $template ) ) {
			$this->template = $template;
		} elseif ( $template instanceof Template ) {
			$this->template = $template->to_s();
		}
		$this->parse();
	}

	/**
	 * Mints a template.
	 *
	 * @param mixed $n input.
	 */
	public function mint( $n ) {
		$str  = $this->prefix;
		$str .= $this->n2xdig( $n );

		if ( $this->has_checkdigit() ) {
			$str .= $this->checkdigit( $str );
		}
		return $str;
	}

	/**
	 * Is the string valid against this template string and checksum?
	 *
	 * @param string $str the string to check.
	 * @return boolean
	 */
	public function is_valid( $str ) {
		$matches = array();
		if ( preg_match( $this->get_validation_regex(), $str ) !== 1 || count( $matches ) < 4 ) {
			return false;
		}
		if ( $this->has_checkdigit() ) {
			return $this->checkdigit( $matches[1] ) === $matches[3];
		}

		return true;
	}

	/**
	 * Calculate a checkdigit for the str.
	 *
	 * @param string $str the string.
	 * @return string checkdigit
	 */
	public function checkdigit( $str ) {
		$chars = array_map(
			function ( $x ) {
				$result = array_search( $x, self::$xdigit, true );
				return false === $result ? 0 : $result;
			},
			str_split( $str )
		);

		$char_count = count( $chars );
		for ( $i = 0; $i < $char_count; $i++ ) {
			$chars[ $i ] *= $i + 1;
		}
		$idx = array_reduce(
			$chars,
			function ( $sum, $n ) {
				return $sum + $n;
			},
			0
		);

		return self::$xdigit[ $idx % count( self::$xdigit ) ];
	}

	/**
	 * Return the minimum sequence value.
	 */
	public function min() {
		if ( null === $this->min ) {
			$this->min = 0;
		}
		return $this->min;
	}

	/**
	 * Template to string.
	 */
	public function to_s() {
		return $this->template;
	}

	/**
	 * Whether this template is equal to another template.
	 *
	 * @param Template $other other template.
	 */
	public function equals( $other ) {
		if ( ! $other instanceof Template ) {
			return false;
		}
		return $this->get_template() === $other->get_template();
	}

	/**
	 * Maximum sequence value for the template.
	 */
	public function max() {
		if ( null === $this->max ) {
			if ( 'z' === $this->generator ) {
				$this->max = null;
			} else {
				$this->max = array_reduce(
					$this->get_size_list(),
					function ( $total, $x ) {
						return $total * $x;
					},
					1
				);
			}
		}
		return $this->max;
	}

	/**
	 * A noid has the structure (prefix)(code)(checkdigit)
	 * the regexp has the following captures
	 *  1 - the prefix and the code
	 *  2 - the changing id characters (not the prefix and not the checkdigit)
	 *  3 - the checkdigit, if there is one. This field is missing if there is no checkdigit
	 */
	protected function get_validation_regex() {
		if ( null === $this->validation_regex ) {
			$character_pattern = '';
			// the first character in the mask after the type character is the most significant
			// acc. to the Noid spec (p.9):
			// https://wiki.ucop.edu/display/Curation/NOID?preview=/16744482/16973835/noid.pdf
			if ( 'z' === $this->generator ) {
				$character_pattern .= $this->character_to_pattern( $this->character_list[0] ) . '*';
			}
			$character_pattern .= implode(
				'',
				array_map(
					function ( $c ) {
						return $this->character_to_pattern( $c );
					},
					$this->character_list
				)
			);

			$this->validation_regex = '/^(' . preg_quote( $this->prefix, null ) . '(' . $character_pattern . '))(';
			if ( $this->has_checkdigit() ) {
				$this->validation_regex .= $this->character_to_pattern( 'k' );
			}
			$this->validation_regex .= ')$/';
		}
		return $this->validation_regex;
	}

	/**
	 * Parse template and put the results into instance variables.
	 * raise an exception if there is a parse error
	 *
	 * @throws Exception When the template is malformed.
	 */
	protected function parse() {
		$matches = array();
		if ( preg_match( self::$valid_pattern, $this->template, $matches ) !== 1 || count( $matches ) < 5 ) {
			throw new Exception( 'Malformed noid template "' . $this->template . '"' ); // phpcs:ignore WordPress.Security.EscapeOutput.ExceptionNotEscaped
		}

		$this->prefix     = $matches[1];
		$this->generator  = $matches[2];
		$this->characters = $matches[3];
		$this->checkdigit = 'k' === $matches[4];
	}

	/**
	 * Returns an "extended digit" pattern.
	 */
	protected function xdigit_pattern() {
		if ( null === $this->xdigit_pattern ) {
			$this->xdigit_pattern = '[' . implode( '', self::$xdigit ) . ']';
		}
		return $this->xdigit_pattern;
	}

	/**
	 * Converts a template character to a regex pattern.
	 *
	 * @param char $c the character.
	 */
	protected function character_to_pattern( $c ) {
		switch ( $c ) {
			case 'e':
			case 'k':
				return $this->xdigit_pattern;
			case 'd':
				return '\d';
			default:
				return '';
		}
	}

	/**
	 * Return a list giving the number of possible characters at each position.
	 */
	protected function get_size_list() {
		if ( null === $this->size_list ) {
			$this->size_list = array_map(
				function ( $char ) {
					return $this->character_space( $char );
				},
				$this->character_list()
			);
		}
		return $this->size_list;
	}

	/**
	 * Returns the character list.
	 */
	protected function character_list() {
		return str_split( $this->characters );
	}

	/**
	 * Returns the NOID mask.
	 */
	protected function mask() {
		return $this->generator . $this->characters;
	}

	/**
	 * Whether the template has a checkdigit.
	 */
	protected function has_checkdigit() {
		return $this->checkdigit;
	}

	/**
	 * Total size of a given template character value.
	 *
	 * @param string $c the character.
	 */
	protected function character_space( $c ) {
		switch ( $c ) {
			case 'e':
				return count( self::$xdigit );
			case 'd':
				return 10;
			default:
				return 0;
		}
	}

	/**
	 * Convert a minter position to a noid string under this template.
	 *
	 * @param int $n the position.
	 * @return string
	 * @throws Exception When the sequence pool is exhausted.
	 */
	protected function n2xdig( $n ) {
		$xdig = implode(
			'',
			array_filter(
				array_map(
					function ( $size ) use ( &$n ) {
						$value = $n % $size;
						$n     = floor( $n / $size );
						return self::$xdigit[ $value ];
					},
					array_reverse( $this->get_size_list() )
				),
				fn( $value ) => ! is_null( $value )
			)
		);

		if ( 'z' === $this->generator ) {
			$size_list = $this->get_size_list();
			$size      = $size_list[ count( $size_list ) - 1 ];
			while ( $n > 0 ) {
				$value = $n % $size;
				$n     = floor( $n / $size );
				$xdig .= self::$xdigit[ $value ];
			}
		}

		if ( $n > 0 ) {
			throw new Exception( 'Exhausted noid sequence pool' );
		}

		return strrev( $xdig );
	}
}
