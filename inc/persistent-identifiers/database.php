<?php
/**
 * Database utils for persistent identifiers.
 *
 * @package GM_Academic
 */

/**
 * Returns the table name of the PID table.
 *
 * @return string
 */
function gm_academic_get_pid_table_name() {
	global $wpdb;

	$table_prefix = $wpdb->prefix;
	return $table_prefix . 'gm_academic_persistent_identifiers';
}

/**
 * Performs the database setup.
 */
function gm_academic_pid_db_setup() {
	global $wpdb;
	global $gm_academic_db_version;

	$installed_version = intval( get_option( GM_ACADEMIC_DB_VERSION, '0' ) );

	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = gm_academic_get_pid_table_name();

	$sql = "CREATE TABLE $table_name (
		id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
		post_id BIGINT UNSIGNED NOT NULL,
		pid VARCHAR(70) NOT NULL,
		pid_type VARCHAR(50) NOT NULL,
		is_current TINYINT(1) DEFAULT 0 NOT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (post_id) REFERENCES {$wpdb->posts} (ID)
	) $charset_collate;";

	require_once ABSPATH . 'wp-admin/includes/upgrade.php';
	dbDelta( $sql );

	if ( 0 === $installed_version ) {
		// new database
		add_option( GM_ACADEMIC_DB_VERSION, $gm_academic_db_version );
	} elseif ( $installed_version < $gm_academic_db_version ) {
		// database updated
		update_option( GM_ACADEMIC_DB_VERSION, $gm_academic_db_version );
	}
}

/**
 * Returns the post ID for a given PID.
 * If the PID is not found or it is not the current PID of a post returns null.
 *
 * @param string $pid the persistent identifier.
 * @return integer|null
 */
function gm_academic_get_post_id_by_current_pid( $pid ) {
	global $wpdb;

	$result = $wpdb->get_results(
		$wpdb->prepare(
			'SELECT post_id FROM %i WHERE pid = %s AND is_current = 1', // phpcs:ignore WordPress.DB.PreparedSQLPlaceholders.UnsupportedIdentifierPlaceholder
			array( gm_academic_get_pid_table_name(), $pid )
		)
	);

	return empty( $result ) ? null : $result[0]->post_id;
}

/**
 * Returns the current (is_current = 1) PID from the given post ID.
 *
 * @param integer $post_id the post id.
 * @return stdClass|null
 */
function gm_academic_get_current_pid_from_post( $post_id ) {
	global $wpdb;

	$result = $wpdb->get_results(
		$wpdb->prepare(
			'SELECT pid, pid_type FROM %i WHERE post_id = %d AND is_current = 1', // phpcs:ignore WordPress.DB.PreparedSQLPlaceholders.UnsupportedIdentifierPlaceholder
			array( gm_academic_get_pid_table_name(), $post_id )
		)
	);

	return empty( $result ) ? null : $result[0];
}

/**
 * Adds the given PID as current PID to the given post ID.
 * Unsets any other current (is_current = 1) PID.
 *
 * @param integer $post_id the post id.
 * @param string  $pid the PID.
 * @param string  $type the type of the PID.
 * @return boolean
 */
function gm_academic_save_pid_for_post( $post_id, $pid, $type ) {
	global $wpdb;

	$current_id = gm_academic_get_id_of_current_pid_from_post( $post_id );
	if ( null !== $current_id ) {
		$wpdb->query(
			$wpdb->prepare(
				'UPDATE %i SET is_current = 0 WHERE id = %d', // phpcs:ignore WordPress.DB.PreparedSQLPlaceholders.UnsupportedIdentifierPlaceholder
				array( gm_academic_get_pid_table_name(), $current_id )
			)
		);
	}

	return $wpdb->query(
		$wpdb->prepare(
			'INSERT INTO %i (post_id, pid, pid_type, is_current) VALUES (%d, %s, %s, %d)', // phpcs:ignore WordPress.DB.PreparedSQLPlaceholders.UnsupportedIdentifierPlaceholder
			array( gm_academic_get_pid_table_name(), $post_id, $pid, $type, 1 )
		)
	);
}

/**
 * Returns the record ID of the current (is_current = 1) PID from the given post ID.
 *
 * @param integer $post_id the post ID.
 * @return integer|null
 */
function gm_academic_get_id_of_current_pid_from_post( $post_id ) {
	global $wpdb;

	$result = $wpdb->get_results(
		$wpdb->prepare(
			'SELECT id FROM %i WHERE post_id = %d AND is_current = 1', // phpcs:ignore WordPress.DB.PreparedSQLPlaceholders.UnsupportedIdentifierPlaceholder
			array( gm_academic_get_pid_table_name(), $post_id )
		)
	);

	return empty( $result ) ? null : $result[0]->id;
}
