<?php
/**
 * Add support for persistent identifiers.
 *
 * @package GM_Academic
 */

// constant for the persistent identifier options page
define( 'GM_ACADEMIC_PID_OPTIONS_PAGE', 'gm_academic_pid' );

// constant for the setting whether pid is active
define( 'GM_ACADEMIC_PID_GENERAL_ACTIVE', 'gm_academic_pid_general_active' );

// constant for the setting which pid type should be used
define( 'GM_ACADEMIC_PID_GENERAL_TYPE', 'gm_academic_pid_general_type' );

// constant for the setting ARK NAAN
define( 'GM_ACADEMIC_PID_ARK_NAAN', 'gm_academic_pid_ark_naan' );

// constant for the setting ARK NOID
define( 'GM_ACADEMIC_PID_ARK_NOID', 'gm_academic_pid_ark_noid' );

// constant for the ARK minter state
define( 'GM_ACADEMIC_PID_ARK_MINTER_STATE', 'gm_academic_pid_ark_minter_state' );

// constant for the PID query variable
define( 'GM_ACADEMIC_PID_QUERY_VAR', 'pid' );

define( 'GM_ACADEMIC_PID_ARK_PATTERN', 'ark\:?\/([0-9]+\/[0-9a-z]+)\/?' );

/**
 * Settings for persistent identifiers.
 */
require plugin_dir_path( __FILE__ ) . 'settings.php';

/**
 * Database related stuff.
 */
require plugin_dir_path( __FILE__ ) . 'database.php';

/**
 * NOID minter.
 */
require plugin_dir_path( __FILE__ ) . 'ark/class-minter.php';

/**
 * Sets a persistent identifier for a given post ID of the custom post type of this plugin
 * if persistent identifiers are active and no persistent identifier has been set yet.
 *
 * @param integer $post_id the post ID.
 */
function gm_academic_pid_on_publish_post( $post_id ) {
	$current_pid = gm_academic_get_current_pid_from_post( $post_id );
	// we do not have a PID yet, create one
	if ( null === $current_pid ) {
		switch ( gm_academic_pid_type() ) {
			case 'ARK':
			default:
				gm_academic_pid_save_ark( $post_id );
		}
	}
}

// do nothing if persistent identifiers feature is not enabled or they are not active
if ( gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_PID ) && gm_academic_pid_is_active() ) {
	add_action( 'publish_' . GM_ACADEMIC_CUSTOM_POST_ARTICLE, 'gm_academic_pid_on_publish_post' );
	add_action( 'future_' . GM_ACADEMIC_CUSTOM_POST_ARTICLE, 'gm_academic_pid_on_publish_post' );

	add_action( 'template_redirect', 'gm_academic_pid_redirect' );
	add_filter( 'preview_post_link', 'gm_academic_pid_preview_link' );

	if ( gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_ARTICLE ) ) {
		add_filter( 'post_type_link', 'gm_academic_pid_article_permalink', 10, 2 );
	}

	add_filter( 'ppl_transform_query_vars', 'gm_academic_pid_ppl_query_var' );
}

/**
 * Generates an ARK persistent identifier for a post and saves it into the database.
 *
 * @param integer $post_id the post ID.
 */
function gm_academic_pid_save_ark( $post_id ) {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE );
	// fetch minter state from db
	$minter_state = get_option( GM_ACADEMIC_PID_ARK_MINTER_STATE );
	if ( false === $minter_state ) {
		$minter_state = array(
			'template' => $options[ GM_ACADEMIC_PID_ARK_NOID ],
		);
		add_option( GM_ACADEMIC_PID_ARK_MINTER_STATE, $minter_state );
	}
	// instantiate minter
	$minter = new Minter();
	$minter->initialize( $minter_state );
	// generate pid
	$pid = $minter->mint();
	// save pid to db
	gm_academic_save_pid_for_post( $post_id, $options[ GM_ACADEMIC_PID_ARK_NAAN ] . '/' . $pid, 'ARK' );
	// save minter state to db
	update_option( GM_ACADEMIC_PID_ARK_MINTER_STATE, $minter->dump() );
}

/**
 * Redirects to post if persistent identifier is queried.
 */
function gm_academic_pid_redirect() {
	$pid = get_query_var( GM_ACADEMIC_PID_QUERY_VAR, null );
	if ( null !== $pid ) {
		global $wp_query;
		$post_id = gm_academic_get_post_id_by_current_pid( $pid );
		if ( null === $post_id ) {
			$wp_query->set_404();
			status_header( 404 );
			get_template_part( 404 );
			exit;
		}

		$wp_query->query( 'page_id=' . $post_id . '&post_type=' . GM_ACADEMIC_CUSTOM_POST_ARTICLE );
		$wp_query->is_single = true;
	}
}

/**
 * Workaround for PID redirect problem for preview URLs: replace PID by post ID.
 *
 * @param string $preview_link the link.
 */
function gm_academic_pid_preview_link( $preview_link ) {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE, array() );
	$url_parts = wp_parse_url( $preview_link );

	$pid = '';
	switch ( $options[ GM_ACADEMIC_PID_GENERAL_TYPE ] ) {
		case 'ARK':
		default:
			$pid = str_replace( '/ark:/', '', $url_parts['path'] );
	}

	$post_id = gm_academic_get_post_id_by_current_pid( $pid );

	return $url_parts['scheme'] . '://' . $url_parts['host'] . '?p=' . $post_id . '&' . $url_parts['query'];
}

/**
 * Changes the permalink of GM Academic custom posts if persistent identifiers are active.
 *
 * @param string  $url the original permalink.
 * @param WP_Post $post the post object.
 * @return string
 */
function gm_academic_pid_article_permalink( $url, $post ) {
	if ( GM_ACADEMIC_CUSTOM_POST_ARTICLE === get_post_type( $post->ID ) ) {
		$pid = gm_academic_get_current_pid_from_post( $post->ID );
		if ( null !== $pid ) {
			$url = get_site_url() . '/' . gm_academic_pid_to_string( $pid );
		}
	}

	return $url;
}

/**
 * Whether persistent identifiers are active.
 *
 * @return boolean
 */
function gm_academic_pid_is_active() {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE, array() );
	return array_key_exists( GM_ACADEMIC_PID_GENERAL_ACTIVE, $options ) && 'true' === $options[ GM_ACADEMIC_PID_GENERAL_ACTIVE ];
}

/**
 * Returns the type of persistent identifier.
 *
 * @return string
 */
function gm_academic_pid_type() {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE );
	return $options[ GM_ACADEMIC_PID_GENERAL_TYPE ];
}

/**
 * Whether a post has a persistent identifier. If persistent identifiers are not active
 * this is always false.
 *
 * @param integer $post_id the post ID.
 * @return boolean
 */
function gm_academic_has_pid( $post_id ) {
	if ( ! gm_academic_pid_is_active() ) {
		return false;
	}
	return gm_academic_get_id_of_current_pid_from_post( $post_id ) !== null;
}

/**
 * Returns a stable link for a persistent identifier. Differs by PID type.
 *
 * @param stdClass $pid the persistent identifier.
 * @return string
 */
function gm_academic_pid_get_link( $pid ) {
	$result = '';

	if ( null === $pid
		|| ! is_object( $pid )
		|| ! property_exists( $pid, 'pid_type' )
		|| ! property_exists( $pid, 'pid' ) ) {
		return $result;
	}

	switch ( $pid->pid_type ) {
		case 'ARK':
			$result = 'https://identifiers.org/ark:/' . $pid->pid;
	}

	return $result;
}

/**
 * Returns a string representation of a persistent identifier.
 *
 * @param stdClass $pid the persistent identifier.
 * @return string
 */
function gm_academic_pid_to_string( $pid ) {
	$result = '';

	if ( null === $pid
		|| ! is_object( $pid )
		|| ! property_exists( $pid, 'pid_type' )
		|| ! property_exists( $pid, 'pid' ) ) {
		return $result;
	}

	switch ( $pid->pid_type ) {
		case 'ARK':
			$result .= 'ark:/';
	}

	return $result . $pid->pid;
}

/**
 * Initializes PID things that should be done on "init".
 */
function gm_academic_pid_init() {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE, array() );
	if ( gm_academic_pid_is_active() ) {
		add_rewrite_tag( '%' . GM_ACADEMIC_PID_QUERY_VAR . '%', '([^&]+)' );

		$regex = '';
		switch ( $options[ GM_ACADEMIC_PID_GENERAL_TYPE ] ) {
			case 'ARK':
			default:
				$regex = '^' . GM_ACADEMIC_PID_ARK_PATTERN;
		}

		add_rewrite_rule( $regex, 'index.php?' . GM_ACADEMIC_PID_QUERY_VAR . '=$matches[1]', 'top' );
	}
}

/**
 * Transform PID query var to custom post type query var for the per post language plugin.
 *
 * @param array $query_vars existing query variables.
 */
function gm_academic_pid_ppl_query_var( $query_vars ) {
	if ( array_key_exists( GM_ACADEMIC_PID_QUERY_VAR, $query_vars ) ) {
		$post_id = gm_academic_get_post_id_by_current_pid( $query_vars[ GM_ACADEMIC_PID_QUERY_VAR ] );
		if ( null !== $post_id ) {
			unset( $query_vars[ GM_ACADEMIC_PID_QUERY_VAR ] );
			$query_vars['p'] = $post_id; // we already know the post ID so we rewrite to the 'p' query var
		}
	}
	return $query_vars;
}
