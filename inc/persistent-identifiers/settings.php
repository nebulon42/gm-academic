<?php
/**
 * Add support for persistent identifiers settings.
 *
 * @package GM_Academic
 */

// constant for the persistent identifier options messages display
define( 'GM_ACADEMIC_PID_OPTIONS_MESSAGES', 'gm_academic_pid_messages' );

// constant for the setting section "general"
define( 'GM_ACADEMIC_PID_SECTION_GENERAL', 'gm_academic_pid_general' );

// constant for the setting section "ARK settings"
define( 'GM_ACADEMIC_PID_SECTION_ARK', 'gm_academic_pid_ark' );

/**
 * Register settings for persistent identifiers.
 */
function gm_academic_pid_settings() {
	if ( gm_academic_feature_enabled( GM_ACADEMIC_OPTS_FEATURES_PID ) ) {
		add_options_page(
			__( 'Persistent Identifiers', 'gm-academic' ),
			__( 'Persistent Identifiers', 'gm-academic' ),
			'manage_options',
			GM_ACADEMIC_PID_OPTIONS_PAGE,
			'gm_academic_pid_options_page'
		);
	}

	register_setting(
		GM_ACADEMIC_PID_OPTIONS_PAGE,
		GM_ACADEMIC_PID_OPTIONS_PAGE,
		array(
			'type'              => 'array',
			'default'           => array(
				GM_ACADEMIC_PID_GENERAL_ACTIVE => 'false',
				GM_ACADEMIC_PID_GENERAL_TYPE   => 'ARK',
			),
			'sanitize_callback' => 'gm_academic_pid_validate_settings',
		)
	);

	add_settings_section(
		GM_ACADEMIC_PID_SECTION_GENERAL,
		__( 'General', 'gm-academic' ),
		'gm_academic_pid_general_rendering',
		GM_ACADEMIC_PID_OPTIONS_PAGE
	);

	add_settings_field(
		GM_ACADEMIC_PID_GENERAL_ACTIVE,
		__( 'Use persistent identifiers', 'gm-academic' ),
		'gm_academic_pid_general_active_rendering',
		GM_ACADEMIC_PID_OPTIONS_PAGE,
		GM_ACADEMIC_PID_SECTION_GENERAL,
		array( 'label_for' => GM_ACADEMIC_PID_GENERAL_ACTIVE )
	);

	add_settings_field(
		GM_ACADEMIC_PID_GENERAL_TYPE,
		__( 'Type of persistent identifier', 'gm-academic' ),
		'gm_academic_pid_general_type_rendering',
		GM_ACADEMIC_PID_OPTIONS_PAGE,
		GM_ACADEMIC_PID_SECTION_GENERAL,
		array( 'label_for' => GM_ACADEMIC_PID_GENERAL_TYPE )
	);

	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE );
	if ( ! isset( $options[ GM_ACADEMIC_PID_GENERAL_TYPE ] )
		|| 'ARK' === $options[ GM_ACADEMIC_PID_GENERAL_TYPE ] ) {
		gm_academic_pid_ark_options();
	}
}
add_action( 'admin_menu', 'gm_academic_pid_settings' );

/**
 * Display PID options page.
 */
function gm_academic_pid_options_page() {
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( esc_html_e( 'You do not have sufficient permissions to access this page.' ) );
	}
	?>
<div class="wrap">
	<h1> <?php esc_html_e( 'Persistent Identifiers Settings', 'gm-academic' ); ?> </h1>
	<form method="POST" action="options.php">
	<?php
	settings_fields( GM_ACADEMIC_PID_OPTIONS_PAGE );
	do_settings_sections( GM_ACADEMIC_PID_OPTIONS_PAGE );
	submit_button();
	?>
	</form>
</div>
	<?php
}

/**
 * Render intro text.
 */
function gm_academic_pid_general_rendering() {
	?>
	<p>
		<?php echo esc_html_e( 'Activating persistent identifiers uses them for the custom post type Article only.', 'gm-academic' ); ?>
	</p>
	<?php
}

/**
 * Render setting for activating and deactivating.
 */
function gm_academic_pid_general_active_rendering() {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE );
	?>
	<input type="radio" id="gm_academic_pid_general_active" name="<?php echo esc_attr( GM_ACADEMIC_PID_OPTIONS_PAGE ); ?>[<?php echo esc_attr( GM_ACADEMIC_PID_GENERAL_ACTIVE ); ?>]" value="true" <?php checked( 'true', $options[ GM_ACADEMIC_PID_GENERAL_ACTIVE ] ); ?>>
	<label for="gm_academic_pid_general_active"><?php echo esc_html_e( 'Yes', 'gm-academic' ); ?></label>
	<input type="radio" id="gm_academic_pid_general_not_active" name="<?php echo esc_attr( GM_ACADEMIC_PID_OPTIONS_PAGE ); ?>[<?php echo esc_attr( GM_ACADEMIC_PID_GENERAL_ACTIVE ); ?>]" value="false" <?php checked( 'false', $options[ GM_ACADEMIC_PID_GENERAL_ACTIVE ] ); ?>>
	<label for="gm_academic_pid_general_not_active"><?php echo esc_html_e( 'No', 'gm-academic' ); ?></label>
	<?php
}

/**
 * Render setting for PID type.
 */
function gm_academic_pid_general_type_rendering() {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE );
	if ( ! array_key_exists( GM_ACADEMIC_PID_GENERAL_TYPE, $options ) ) {
		$options[ GM_ACADEMIC_PID_GENERAL_TYPE ] = 'ARK';
	}
	?>
	<select name="<?php echo esc_attr( GM_ACADEMIC_PID_OPTIONS_PAGE ); ?>[<?php echo esc_attr( GM_ACADEMIC_PID_GENERAL_TYPE ); ?>]">
		<option value="ARK" <?php selected( $options[ GM_ACADEMIC_PID_GENERAL_TYPE ], 'ARK' ); ?>>ARK</option>
	</select>
	<?php
}

/**
 * Display settings for ARKs.
 */
function gm_academic_pid_ark_options() {
	add_settings_section(
		GM_ACADEMIC_PID_SECTION_ARK,
		__( 'ARK settings', 'gm-academic' ),
		'gm_academic_pid_ark_rendering',
		GM_ACADEMIC_PID_OPTIONS_PAGE
	);

	add_settings_field(
		GM_ACADEMIC_PID_ARK_NAAN,
		__( 'Name Assigning Authority Number (NAAN)', 'gm-academic' ),
		'gm_academic_pid_ark_naan_rendering',
		GM_ACADEMIC_PID_OPTIONS_PAGE,
		GM_ACADEMIC_PID_SECTION_ARK,
		array( 'label_for' => GM_ACADEMIC_PID_ARK_NAAN )
	);

	add_settings_field(
		GM_ACADEMIC_PID_ARK_NOID,
		__( 'Nice Opaque Identifier (NOID) template', 'gm-academic' ),
		'gm_academic_pid_ark_noid_rendering',
		GM_ACADEMIC_PID_OPTIONS_PAGE,
		GM_ACADEMIC_PID_SECTION_ARK,
		array( 'label_for' => GM_ACADEMIC_PID_ARK_NOID )
	);
}

/**
 * Render ARK introduction text.
 */
function gm_academic_pid_ark_rendering() {
	?>
	<p>
		<?php echo esc_html_e( 'Changes to the settings below only affect newly generated persistent identifiers.', 'gm-academic' ); ?>
	</p>
	<?php
}

/**
 * Render ARK NAAN setting.
 */
function gm_academic_pid_ark_naan_rendering() {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE );
	if ( ! array_key_exists( GM_ACADEMIC_PID_ARK_NAAN, $options ) ) {
		$options[ GM_ACADEMIC_PID_ARK_NAAN ] = '';
	}
	?>
	<input type="text" name="<?php echo esc_attr( GM_ACADEMIC_PID_OPTIONS_PAGE ); ?>[<?php echo esc_attr( GM_ACADEMIC_PID_ARK_NAAN ); ?>]" value="<?php echo esc_attr( $options[ GM_ACADEMIC_PID_ARK_NAAN ] ); ?>">
	<p class="description">
		<?php echo esc_html_e( 'This is a number that gets assigned to you upon request. Visit arks.org for more information.', 'gm-academic' ); ?>
	</p>
	<?php
}

/**
 * Render ARK NOID template setting.
 */
function gm_academic_pid_ark_noid_rendering() {
	$options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE );
	if ( ! array_key_exists( GM_ACADEMIC_PID_ARK_NOID, $options ) ) {
		$options[ GM_ACADEMIC_PID_ARK_NOID ] = '';
	}
	?>
	<input type="text" name="<?php echo esc_attr( GM_ACADEMIC_PID_OPTIONS_PAGE ); ?>[<?php echo esc_attr( GM_ACADEMIC_PID_ARK_NOID ); ?>]" value="<?php echo esc_attr( $options[ GM_ACADEMIC_PID_ARK_NOID ] ); ?>">
	<p class="description">
		<?php
			printf(
				wp_kses(
					/* translators: %s contains an URL */
					__( 'See the <a href="%s" target="_blank">NOID documentation</a> for valid template variants.', 'gm-academic' ),
					'https://metacpan.org/dist/Noid/view/noid#TEMPLATES'
				),
				array(
					'a' => array(
						'href'   => array(),
						'target' => array(),
					),
				)
			);
		?>
	</p>
	<?php
}

/**
 * Validate settings on save.
 *
 * @param array $data the data.
 */
function gm_academic_pid_validate_settings( $data ) {
	$current_options = get_option( GM_ACADEMIC_PID_OPTIONS_PAGE );

	// user wants to activate or keep active
	if ( 'true' === $data[ GM_ACADEMIC_PID_GENERAL_ACTIVE ] ) {
		$problems = '';

		if ( ! isset( $data[ GM_ACADEMIC_PID_GENERAL_TYPE ] )
			|| 'ARK' === $data[ GM_ACADEMIC_PID_GENERAL_TYPE ] ) {
			$problems = gm_academic_pid_validate_settings_ark( $data );
		}

		if ( strlen( $problems ) > 0 ) {
			if ( 'false' === $current_options[ GM_ACADEMIC_PID_GENERAL_ACTIVE ]
				&& 'true' === $data[ GM_ACADEMIC_PID_GENERAL_ACTIVE ] ) {
				// re-set to inactive on errors if activating
				$data[ GM_ACADEMIC_PID_GENERAL_ACTIVE ] = 'false';

				/* translators: %s contains found problems */
				$prefix = __( 'Please correct the following problems before enabling persistent identifiers: %s', 'gm-academic' );
			} else {
				/* translators: %s contains found problems */
				$prefix = __( 'Could not update persistent identifier settings due to the following problems: %s', 'gm-academic' );
			}

			add_settings_error(
				GM_ACADEMIC_PID_OPTIONS_MESSAGES,
				'problems',
				sprintf( $prefix, $problems ),
				'error'
			);
		}
	}

	// if currently active and not deactivating: do not let wrong values be saved
	if ( 'true' === $current_options[ GM_ACADEMIC_PID_GENERAL_ACTIVE ]
		&& 'true' === $data[ GM_ACADEMIC_PID_GENERAL_ACTIVE ] ) {
		$data = $current_options;
	}

	return $data;
}

/**
 * Validate ARK settings on save.
 *
 * @param array $data the data.
 */
function gm_academic_pid_validate_settings_ark( $data ) {
	$has_errors = false;
	$problems   = '<ul>';

	if ( empty( $data[ GM_ACADEMIC_PID_ARK_NAAN ] ) ) {
		$problems  .= '<li>' . __( 'NAAN needs a value.', 'gm-academic' ) . '</li>';
		$has_errors = true;
	}
	if ( ! preg_match( '/^[0-9]{5,}$/', $data[ GM_ACADEMIC_PID_ARK_NAAN ] ) ) {
		$problems  .= '<li>' . __( 'NAAN needs to be an integer of at least 5 digits.', 'gm-academic' ) . '</li>';
		$has_errors = true;
	}

	if ( empty( $data[ GM_ACADEMIC_PID_ARK_NOID ] ) ) {
		$problems  .= '<li>' . __( 'NOID template needs a value.', 'gm-academic' ) . '</li>';
		$has_errors = true;
	}
	if ( ! preg_match( '/^([a-z0-9]+)?\.([rsz][de]+k?)$/', $data[ GM_ACADEMIC_PID_ARK_NOID ] ) ) {
		$problems  .= '<li>' . __( 'NOID template needs to match ([a-z0-9]+)?\.([rsz][de]+k?).', 'gm-academic' ) . '</li>';
		$has_errors = true;
	}

	$problems .= '</ul>';

	return $has_errors ? $problems : '';
}