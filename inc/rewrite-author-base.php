<?php
/**
 * Add support for changing the author base.
 *
 * @package GM_Academic
 */

/**
 * Rewrite author base to custom
 */
add_action(
	'init',
	function () {
		global $wp_rewrite;
		$author_base_db = get_option( 'gm_academic_author_base' );
		if ( ! empty( $author_base_db ) ) {
			$wp_rewrite->author_base = $author_base_db;
		}
	}
);

/**
 * Render textinput for Author base
 * Callback for the add_settings_function()
 */
function gm_academic_author_base_render_field() {
	global $wp_rewrite;
	printf(
		'<input name="gm_academic_author_base" id="lwp_author_base" type="text" value="%s" class="regular-text code">',
		esc_attr( $wp_rewrite->author_base )
	);
}

/**
 * Add a setting field for Author Base to the "Optional" Section of the Permalinks Page.
 */
add_action(
	'admin_init',
	function () {
		add_settings_field(
			'gm_academic_author_base',
			esc_html__( 'Author base', 'gm-academic' ),
			'gm_academic_author_base_render_field',
			'permalink',
			'optional',
			array( 'label_for' => 'gm_academic_author_base' )
		);
	}
);

/**
 * Sanitize and save the given Author Base value to the database
 */
add_action(
	'admin_init',
	function () {
		$author_base_db = get_option( 'gm_academic_author_base' );

		if ( isset( $_POST['gm_academic_author_base'] ) &&
			isset( $_POST['permalink_structure'] ) &&
			check_admin_referer( 'update-permalink' )
		) {
			$author_base = sanitize_title( wp_unslash( $_POST['gm_academic_author_base'] ) );

			if ( empty( $author_base ) ) {
				add_settings_error(
					'gm_academic_author_base',
					'gm_academic_author_base',
					esc_html__( 'Invalid Author Base.', 'gm-academic' ),
					'error'
				);
			} elseif ( $author_base_db !== $author_base ) {
				update_option( 'gm_academic_author_base', $author_base );
			}
		}
	}
);
