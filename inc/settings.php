<?php
/**
 * Add plugin settings.
 *
 * @package GM_Academic
 */

// constant for the persistent identifier options page
define( 'GM_ACADEMIC_OPTIONS_PAGE', 'gm_academic_opts' );

// constant for the setting section "features"
define( 'GM_ACADEMIC_OPTS_SECTION_FEATURES', 'gm_academic_opts_features' );

// constant for the setting whether pid is active
define( 'GM_ACADEMIC_OPTS_FEATURES_PID', 'gm_academic_opts_features_pid' );

// constant for the setting whether custom post type article is active
define( 'GM_ACADEMIC_OPTS_FEATURES_ARTICLE', 'gm_academic_opts_features_article' );

// constant for the setting whether licenses are active
define( 'GM_ACADEMIC_OPTS_FEATURES_LICENSES', 'gm_academic_opts_features_licenses' );

/**
 * Registers the options page and settings.
 */
function gm_academic_register_options() {
	add_options_page(
		__( 'GM Academic', 'gm-academic' ),
		__( 'GM Academic', 'gm-academic' ),
		'manage_options',
		GM_ACADEMIC_OPTIONS_PAGE,
		'gm_academic_options_page'
	);

	register_setting(
		GM_ACADEMIC_OPTIONS_PAGE,
		GM_ACADEMIC_OPTS_SECTION_FEATURES,
		array(
			'type'              => 'array',
			'default'           => array(
				GM_ACADEMIC_OPTS_FEATURES_LICENSES => 'false',
				GM_ACADEMIC_OPTS_FEATURES_ARTICLE  => 'false',
				GM_ACADEMIC_OPTS_FEATURES_PID      => 'false',
			),
			'sanitize_callback' => 'gm_academic_validate_settings',
		)
	);

	add_settings_section(
		GM_ACADEMIC_OPTS_SECTION_FEATURES,
		__( 'Features', 'gm-academic' ),
		'gm_academic_opts_features_rendering',
		GM_ACADEMIC_OPTIONS_PAGE
	);

	add_settings_field(
		GM_ACADEMIC_OPTS_FEATURES_LICENSES,
		__( 'Enable post and media licenses', 'gm-academic' ),
		'gm_academic_opts_features_licenses_rendering',
		GM_ACADEMIC_OPTIONS_PAGE,
		GM_ACADEMIC_OPTS_SECTION_FEATURES,
		array( 'label_for' => GM_ACADEMIC_OPTS_FEATURES_LICENSES )
	);

	add_settings_field(
		GM_ACADEMIC_OPTS_FEATURES_ARTICLE,
		__( 'Enable Article post type', 'gm-academic' ),
		'gm_academic_opts_features_article_rendering',
		GM_ACADEMIC_OPTIONS_PAGE,
		GM_ACADEMIC_OPTS_SECTION_FEATURES,
		array( 'label_for' => GM_ACADEMIC_OPTS_FEATURES_ARTICLE )
	);

	add_settings_field(
		GM_ACADEMIC_OPTS_FEATURES_PID,
		__( 'Enable persistent identifiers', 'gm-academic' ),
		'gm_academic_opts_features_pid_rendering',
		GM_ACADEMIC_OPTIONS_PAGE,
		GM_ACADEMIC_OPTS_SECTION_FEATURES,
		array( 'label_for' => GM_ACADEMIC_OPTS_FEATURES_PID )
	);
}
add_action( 'admin_menu', 'gm_academic_register_options' );

/**
 * Display options page.
 */
function gm_academic_options_page() {
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( esc_html_e( 'You do not have sufficient permissions to access this page.' ) );
	}
	?>
<div class="wrap">
	<h1> <?php esc_html_e( 'GM Academic Settings', 'gm-academic' ); ?> </h1>
	<form method="POST" action="options.php">
	<?php
	settings_fields( GM_ACADEMIC_OPTIONS_PAGE );
	do_settings_sections( GM_ACADEMIC_OPTIONS_PAGE );
	submit_button();
	?>
	</form>
</div>
	<?php
}

/**
 * Render intro text for section "features".
 */
function gm_academic_opts_features_rendering() {
	?>
	<p>
		<?php echo esc_html_e( 'Choose below which features you want to enable.', 'gm-academic' ); ?>
	</p>
	<?php
}

/**
 * Render setting for Licenses feature.
 */
function gm_academic_opts_features_licenses_rendering() {
	gm_academic_opts_feature_chooser( GM_ACADEMIC_OPTS_FEATURES_LICENSES );
}

/**
 * Render setting for Article feature.
 */
function gm_academic_opts_features_article_rendering() {
	gm_academic_opts_feature_chooser( GM_ACADEMIC_OPTS_FEATURES_ARTICLE );
}

/**
 * Render setting for PID feature.
 */
function gm_academic_opts_features_pid_rendering() {
	gm_academic_opts_feature_chooser( GM_ACADEMIC_OPTS_FEATURES_PID );
}

/**
 * Render setting for feature activating and deactivating.
 *
 * @param string $option option to render.
 */
function gm_academic_opts_feature_chooser( $option ) {
	$options = get_option( GM_ACADEMIC_OPTS_SECTION_FEATURES );
	?>
	<input type="radio" id="<?php echo esc_attr( $option ); ?>_active" name="<?php echo esc_attr( GM_ACADEMIC_OPTS_SECTION_FEATURES ); ?>[<?php echo esc_attr( $option ); ?>]" value="true" <?php checked( 'true', $options[ $option ] ); ?>>
	<label for="<?php echo esc_attr( $option ); ?>_active"><?php echo esc_html_e( 'Yes', 'gm-academic' ); ?></label>
	<input type="radio" id="<?php echo esc_attr( $option ); ?>_not_active" name="<?php echo esc_attr( GM_ACADEMIC_OPTS_SECTION_FEATURES ); ?>[<?php echo esc_attr( $option ); ?>]" value="false" <?php checked( 'false', $options[ $option ] ); ?>>
	<label for="<?php echo esc_attr( $option ); ?>_not_active"><?php echo esc_html_e( 'No', 'gm-academic' ); ?></label>
	<?php
}

/**
 * Validate settings on save.
 *
 * @param array $data the data.
 */
function gm_academic_validate_settings( $data ) {
	$current_options = get_option( GM_ACADEMIC_OPTS_SECTION_FEATURES );

	return $data;
}

/**
 * Returns whether the given feature is enabled.
 *
 * @param string $feature the name of the feature to check.
 * @return bool
 */
function gm_academic_feature_enabled( $feature ) {
	$options = get_option( GM_ACADEMIC_OPTS_SECTION_FEATURES, array() );
	return array_key_exists( $feature, $options ) && 'true' === $options[ $feature ];
}
