(function ($) {
    $('body').on('click', '#gm-academic-license-selection label', function (e) {
        if (e.target.tagName.toLowerCase() !== 'a') {
            $(this).parent().find('input[type="radio"]').prop('checked', true);
        }
    });
})(jQuery);